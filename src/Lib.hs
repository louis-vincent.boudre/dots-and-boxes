module Lib
    ( 
      someFunc
    ) where

import Simulation
import Game

someFunc :: IO ()
someFunc = do 
    let game = buildGridGame 2 2
    let sim = buildRandomSim (14, 32) game
    print $ (++) "win margin: " $ show $ exec sim
