module Solver
    ( 
      minimax
    ) where

import Box
import Game
import Graph (Edge)
import Utils
import Data.Tuple
import Data.Hashable
import Data.Tree
import qualified Data.List as L
import qualified Data.Array as A
import qualified Data.HashMap.Strict as H
import Data.Function (on)

------------
-- Solver -- 
------------
sortMoves :: Tree Game -> Tree Game
sortMoves tree@(Node game []) = tree
sortMoves (Node game forest) = (Node game forest') 
    where forest' = map snd $ L.sortBy (flip compare `on` fst) xs 
          xs = [(eagerPlay g, subtree) | subtree@(Node g _) <- forest]


-- |Chain all possible boxes then select an edge that minimize box creation.
eagerPlay :: Game -> Int
eagerPlay game
    | edges == [] = winMargin game'
    | otherwise = eagerPlay game'
    where game' = foldr updateGame game edges
          edges = [edge | (_, edge) <- closeables (boxes game)]

minimizeOther :: Game -> Game
minimizeOther game
    | moves game == [] = game
    | otherwise = snd $ L.minimumBy (compare `on` fst) xs
        where xs = [( (availableBoxCount (boxes g)) , g) | g <- spanGame game]

-- |Executes minimax with alpha-beta pruning
-- 
-- >>> minimax $ buildGridGame 1 1
-- -1 
minimax :: Game -> Int
minimax game = minimax' (buildGameTree game) minBound maxBound

-- | minimax util function
--
-- >>> let game = buildGridGame 1 1
-- >>> let tree = buildGameTree $ foldr updateGame game [(1,2),(1,3),(3,4)]
-- >>> player (rootLabel tree)
-- Y
-- >>> let (alpha, beta) = (minBound::Int, maxBound::Int)
-- >>> minimax' tree alpha beta
-- -1
-- >>> let tree2 = buildGameTree $ foldr updateGame game [(1,2),(1,3)]
-- >>> player (rootLabel tree2)
-- X
-- >>> minimax' tree2 alpha beta
-- -1
minimax' :: Tree Game -> Int -> Int -> Int
minimax' (Node game []) _ _ = winMargin game
minimax' tree a b = trd $ foldr alphabetaPrune (a,b,val) forest'
    where val = if' ((player game) == X) minBound maxBound
          (Node game forest') = sortMoves tree

-- |Prune using alpha-beta strategy
-- 
-- >>> let game = buildGridGame 1 1
-- >>> let tree = buildGameTree $ foldr updateGame game [(1,2),(1,3),(3,4)]
-- >>> let (alpha, beta) = (minBound::Int, maxBound::Int)
-- >>> (minBound, -1, -1) == alphabetaPrune tree (alpha, beta, maxBound)
-- True 
-- >>> let tree2 = buildGameTree $ foldr updateGame game [(1,2),(1,3)]
-- >>> trd $ alphabetaPrune tree2 (alpha, beta, minBound)
-- -1
alphabetaPrune :: Tree Game -> (Int, Int, Int) -> (Int, Int, Int)
alphabetaPrune tree@(Node game _) (a,b,x)
    | b <= a = (a,b,x)
    | otherwise = (a', b', e')
        where pl = player game
              e  = if' (pl == X) minBound maxBound
              m  = if' (pl == X) max min 
              e' = m (minimax' tree a b) e 
              a' = if' (pl == X) (max e' a) a
              b' = if' (pl == Y) (min e' b) b

solve :: Int
solve = minimax $ buildGridGame 3 3

