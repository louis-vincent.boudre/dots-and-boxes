module Box
    (
        Box,
        BoxPartition,
        boxPartition,
        closeableBy,
        closeables,
        toCloseables,
        updatePartition,
        fromFaces,
        availableBoxCount,
        openChains,
        halfOpenChains,
        isCloseable
    ) where

import Graph
import Utils 
import Data.Maybe
import Data.Array
import qualified Data.HashSet as HashSet
import qualified Data.List as L (delete, last, intercalate, unfoldr, find)
import Control.Monad (guard)
type Box = Int
type BoxPartition = Array Box (HashSet.HashSet Edge) 
type Chain = [Box] 

-- |Creates a BoxPartition from a planar graph
boxPartition :: Graph -> BoxPartition
boxPartition = fromFaces.faces

-- |Creates a BoxPartition from faces
-- >>> length $ fromFaces $ snd $ gridGWithFaces 1 1
-- 1
fromFaces :: Faces -> BoxPartition
fromFaces faces = array (1, length classes) $ zip [1..] classes
    where classes = fmap HashSet.fromList faces


availableBoxCount :: BoxPartition -> Int
availableBoxCount = length.closeables

-- |Boxes that become closeable due to an edge
toCloseables :: Edge -> BoxPartition -> [Edge]
toCloseables edge bp = map snd $ closeables bp'
    where bp' = updatePartition edge bp 


-- |Returns all edges that close a box
closeables :: BoxPartition -> [(Box, Edge)]
closeables bp = [(box, pickAny set) | (box,set) <- assocs bp, length set == 1]

-- |Returns boxes that would be closed by an edge
closeableBy :: Edge -> BoxPartition -> [Box]
closeableBy edge bp = [i | (i,set) <- assocsOf (boxesOf edge bp) bp, length set == 1] 

-- |Same behavior as `assocs`, except it selects from a set of indices.
assocsOf :: Ix i => [i] -> Array i e -> [(i,e)]
assocsOf is arr = [(i, arr ! i) | i <- is]

-- |Returns all the boxes containing an edge
--
-- >>> let bp = fromFaces $ snd $ gridGWithFaces 1 2
-- >>> boxesOf (5,2) bp
-- [1,2]
boxesOf:: Edge -> BoxPartition -> [Box]
boxesOf (u,v) bp = [i | (i,set) <- assocs bp, edge `HashSet.member` set]
    where edge = if' (v < u) (v,u) (u,v)

-- |Update the current box partition by removing an edge
--
-- >>> let (graph, faces) = gridGWithFaces 2 2
-- >>> let bp = fromFaces faces
-- >>> foldr updatePartition bp (edges graph)
-- array (1,4) [(1,fromList []),(2,fromList []),(3,fromList []),(4,fromList [])]
updatePartition :: Edge -> BoxPartition -> BoxPartition
updatePartition (u,v) bp = fmap (HashSet.delete edge) bp
    where edge = if' (v < u) (v,u) (u,v)

-- |Returns all long and half-open chains
-- A long chain contains at least 3 box, loops don't count as two long chains.

-- >>> let (graph, faces) = gridGWithFaces 3 3
-- >>> let bp = fromFaces faces
-- >>> let bp' = foldr updatePartition bp [(1,2),(1,5),(2,6)]
-- >>> halfOpenChains bp'
-- []
-- >>> let bp' = foldr updatePartition bp [(1,2),(1,5),(2,6),(5,9),(6,10),(9,13),(10,14),(13,14)] 
-- >>> halfOpenChains bp'
-- [[1,4,7]]
halfOpenChains :: BoxPartition -> [Chain]
halfOpenChains bp = filter ((> 2).length) $ L.unfoldr f boxSet
    where boxList = map fst (closeables bp)
          boxSet = HashSet.fromList boxList
          f set
            | length set == 0 = Nothing
            | otherwise = let (box, set') = pickAndUpdate set   -- delete current box
                              chain = chainAt box bp
                              last  = L.last chain              -- delete last box to avoid duplicate computation
                              set'' = HashSet.delete last set' in Just (chainAt box bp, set'')

-- |Returns all long and open chains
-- 
-- An open chain must have both ends open (available edge).
-- >>> let bp= fromFaces $ snd $ gridGWithFaces 3 3
-- >>> let bp' = foldr updatePartition bp [(1,2),(1,5),(2,6),(5,9),(6,10),(9,13),(10,14),(13,14)] 
-- >>> openChains bp'
-- []
-- >>> let bp' = foldr updatePartition bp [(1,5),(2,6),(5,9),(6,10),(9,13),(10,14)] 
-- >>> openChains bp'
-- [[1,4,7]]
-- >>> let bp' = foldr updatePartition bp [(1,5),(2,6),(5,9),(6,10),(9,13),(10,14),(13,14)] 
-- >>> openChains bp'
-- []
-- >>> let bp' = foldr updatePartition bp [(2,6),(6,10),(11,10),(11,12),(3,7),(7,8)]
-- >>> openChains bp'
-- [[2,5,6]]
openChains :: BoxPartition -> [Chain]
openChains bp = do
    box <- [i | (i,set) <- assocs bp, length set == 2] 
    edge <- remainingEdgesFor box bp
    let bp' = updatePartition edge bp 
    let chain = chainAt box bp'
    guard (length chain > 2) -- must be a long chain
    let last = L.last chain
    guard (length (remainingEdgesFor last bp) > 1)  -- the last box must have more than one edge available
    guard (last > head chain)   -- it removes duplicates
    return (chain) 

-- |Checks if selecting an edge cause a chain to be half open
willHalfChain :: Edge -> BoxPartition -> Bool
willHalfChain edge bp = length xs > 0 
    where xs = do   -- TODO refactor with openChains
            box <- boxesOf edge bp
            let bp' = updatePartition edge bp
            let chain = chainAt box bp'
            guard (length chain > 2) -- must be a long chain
            let last = L.last chain
            guard (length (remainingEdgesFor last bp) > 1)  -- the last box must have more than one edge available
            guard (last > head chain)   -- it removes duplicates
            return (chain) 

-- |Tries to start a chain at a given box
--  
-- >>> let (graph, faces) = gridGWithFaces 2 2
-- >>> let bp = fromFaces faces
-- >>> let bp' = foldr updatePartition bp [(1,2),(1,4),(2,5),(4,7),(7,8),(8,9),(9,6)]
-- >>> length $ closeableBy (4,5) bp'
-- 1
-- >>> chainAt 1 bp'
-- [1,3,4]
-- >>> let bp = fromFaces $ snd (gridGWithFaces 3 3)
-- >>> let bp' = foldr updatePartition bp [(1,2),(1,5),(2,6),(5,9),(6,10),(9,13),(10,14),(13,14)]
-- >>> chainAt 7 bp' 
-- [7,4,1]
-- >>> let bp' = foldr updatePartition bp [(2,6),(2,3),(6,10),(11,10),(11,12),(3,7),(7,8)]
-- >>> chainAt 2 bp'
-- [2,5,6]
chainAt :: Box -> BoxPartition -> Chain
chainAt box bp
    | isCloseable box bp = 
        if closeCount == 2 
            then [box, box']    -- end of a chain if an edge close two box
            else if length (boxesOf edge bp) == 1
                then [box]
                else box : chainAt box' bp'
    | otherwise = [] 
        where edge = fromJust (nextEdge box bp)
              closeCount = length $ closeableBy edge bp
              box' = head $ L.delete box (boxesOf edge bp)
              bp' = closeBox box bp

-- |Force close a box by empty its partition
closeBox :: Box -> BoxPartition -> BoxPartition
closeBox box bp = foldr updatePartition bp $ remainingEdgesFor box bp     

-- |Returns the remaining edge of a box
-- Remaining edges are edges that had not been taken
remainingEdgesFor :: Box -> BoxPartition -> [Edge]
remainingEdgesFor box bp = HashSet.toList $ bp ! box

isCloseable :: Box -> BoxPartition -> Bool
isCloseable box bp = length (bp ! box) == 1

-- |Determines if two boxes are adjacent, i.e if they share one edge
--
-- >>> let (graph, faces) = gridGWithFaces 3 3
-- >>> let bp = fromFaces faces
-- >>> areAdjacent 1 2 bp
-- True
-- >>> areAdjacent 1 4 bp
-- True
-- >>> areAdjacent 1 3 bp
-- False
-- >>> areAdjacent 5 1 bp
-- False
-- >>> areAdjacent 8 5 bp
-- True
-- >>> areAdjacent 5 8 bp
-- True
areAdjacent :: Box -> Box -> BoxPartition -> Bool
areAdjacent b1 b2 bp = length (set1 `HashSet.intersection` set2) == 1
    where set1 = bp ! b1
          set2 = bp ! b2

-- |Finds the box next to a given box at the edge intersection
-- At least one of the box separated by an edge must be open,
-- otherwise it panics.
--
-- 1-2-3
-- |1|2| 
-- 4-5-6
--
-- >>> let bp = fromFaces $ snd $ gridGWithFaces 1 2
-- >>> nextTo bp (2,5) 1 
-- 2
-- >>> nextTo bp (5,2) 2
-- 1
-- >>> nextTo bp (2,5) 2
-- 1
nextTo :: BoxPartition -> Edge -> Box -> Box
nextTo bp edge box = head $ L.delete box $ boxesOf edge bp 

nextEdge :: Box -> BoxPartition -> Maybe Edge
nextEdge box bp
 | length set == 0 = Nothing
 | otherwise = Just (pickAny set)
    where set = bp ! box

