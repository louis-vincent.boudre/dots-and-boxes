module Game
    ( buildGameTree,
      buildGridGame,
      updateGame,
      winMargin,
      spanGame,
      Game(..),
      moves,
      Score(..),
      Player(..)
    ) where

import Graph
import Box
import Utils
import Data.Tuple
import Data.Hashable
import Data.Tree
import qualified Data.List as L
import qualified Data.Array as A
import qualified Data.HashMap.Strict as H

--------------------
-- Dots and boxes --
--------------------

data Game = Game { 
    graph :: Graph,
    player :: Player,   -- current player to play
    score :: Score,
    boxes :: BoxPartition
}

instance Hashable Game where
    hashWithSalt salt (Game g _ _ _) = hashWithSalt salt g

instance Eq Game where
    (==) (Game g1 _ _ _) (Game g2 _ _ _) = g1 == g2

instance Show Game where
    show (Game graph _ score _) = show score

data Player = X | Y deriving (Show, Eq)

data Score = Score {
  xscore :: Int,
  yscore :: Int
} deriving (Show, Eq)


buildGridGame :: Int -> Int -> Game
buildGridGame n m = (Game graph X (Score 0 0) bp)
    where (graph, faces) = gridGWithFaces n m
          bp = fromFaces faces 

buildGame :: Graph -> Game
buildGame graph = (Game graph X (Score 0 0) (boxPartition graph)) 

-- |Build a lazy game tree
-- The algorithm prune some symmetries
--
-- >>> map player $ Data.Tree.flatten $ buildGameTree $ buildGridGame 1 1
-- [X,Y,X,Y,Y,X,Y,Y]
buildGameTree :: Game -> Tree Game 
buildGameTree game = (Node game forest)
    where forest = [buildGameTree g' | g' <- spanGame game] 

-- |Generates every possible game states from available moves
spanGame :: Game -> [Game]
spanGame game = nubGame [updateGame edge game | edge <- moves game]

nubGame :: [Game] -> [Game]
nubGame games = H.elems $ foldr apply H.empty fns
    where fns = [H.insert (canonizeGame g) g | g <- games]  -- predefined inserts
          apply f hmap = f hmap

-- |Returns available moves to play
moves :: Game -> [Edge]
moves = edges.graph

-- |Canonical form of a game state
canonizeGame :: Game -> Game
canonizeGame (Game graph x y z) = (Game (canonize graph) x y z)

-- |Updates score for a player
-- 
-- >>> let s = (Score 0 0)
-- >>> s == (addScoreFor 0 X s)
-- True
-- >>> addScoreFor 1 X s
-- Score {xscore = 1, yscore = 0} 
-- >>> addScoreFor 3 Y $ addScoreFor 1 X s 
-- Score {xscore = 1, yscore = 3}
-- >>> addScoreFor 2 X $ addScoreFor 3 Y $ addScoreFor 1 X s
-- Score {xscore = 3, yscore = 3}
addScoreFor :: Int -> Player -> Score -> Score
addScoreFor val X (Score x y) = (Score (x + val) y)
addScoreFor val Y (Score x y) = (Score x (y + val))

otherPlayer :: Player -> Player
otherPlayer X = Y
otherPlayer Y = X

-- |Updates a game state
--
-- >>> let game = buildGridGame 1 1
-- >>> score $ foldr updateGame game (moves game)
-- Score {xscore = 0, yscore = 1} 
--
-- >>> let game = buildGridGame 1 2
-- >>> let ms = [(1,2),(2,5),(4,5),(1,4),(2,3),(3,6),(5,6)]
-- >>> score $ foldr updateGame game ms
-- Score {xscore = 0, yscore = 2}
--
-- >>> player $ foldr updateGame game [(1,2),(2,5),(4,5)]
-- Y
-- >>> player $ foldr updateGame game [(1,2),(2,5),(4,5),(1,4)]
-- Y
updateGame :: Edge -> Game -> Game
updateGame edge (Game graph player score bp) = (Game graph' player' score' bp')
    where graph' = rmEdge edge graph
          points = length $ closeableBy edge bp
          player' = if' (points > 0) player (otherPlayer player) 
          score' = addScoreFor points player score
          bp' = updatePartition edge bp

winMargin :: Game -> Int 
winMargin (Game _ _ (Score x y) _) = x - y 
