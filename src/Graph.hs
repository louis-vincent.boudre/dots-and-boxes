--{-# LANGUAGE FlexibleInstances #-}

{-|
Module : Graph
Description : Undirected Wrapper of Data.Graph

This is a wrapper around the Data.Graph type. It adds support for:
- removing edge, a.k.a incrementally updating a graph adjancy list;
- implementation of Hashable typeclasse for HashMap support;
- undirected edge logic when building graph and iterating over edges.
-}
module Graph 
  (
    Graph,
    Edge,
    Vertex,
    Bounds,
    Faces,
    gridG,
    tarjan,
    gridGWithFaces,
    edges,
    edges2,
    vertices,
    neighbors,
    rmEdge,
    faces,
    fromEdges,
    canonize,
    lohi,
    Permutation,
    automorphisms,
    edgesOf
  ) where

import TextShow
import Data.Hashable
import Data.Tuple
import Data.Graph.Automorphism (canonicGraph, autGenerators)
import Data.Graph.Partition (Partition, unitPartition)
import Control.Monad (guard)
import Data.Function (on)
import qualified Data.Graph as G
import qualified Data.List as L
import qualified Data.Array as A
import qualified Data.HashMap.Strict as H
newtype Graph = Graph { inner :: G.Graph } deriving (Show)

-- |Duplicate from Data.Graph, since Haskell has a hard time resolving
-- function signature of type synonym of type synonym
type Edge = (Vertex, Vertex)    
type Vertex = Int
type Bounds = (Vertex, Vertex)
type Permutation = A.Array Vertex Vertex
type Face = [Edge]
type Faces = [Face]

instance Hashable Graph where
  hashWithSalt salt g = hashWithSalt salt $ showt $ edges g

instance Eq Graph where
  (==) g1 g2 = L.sort (edges g1) == L.sort (edges g2)

-- |Builds a grid of n x m boxes
--
-- >>> let g = gridG 2 2
-- >>> vertices g
-- [1,2,3,4,5,6,7,8,9]
-- >>> L.sort $ edges g
-- [(1,2),(1,4),(2,3),(2,5),(3,6),(4,5),(4,7),(5,6),(5,8),(6,9),(7,8),(8,9)]
-- >>> let g1 = gridG 1 1
-- >>> vertices g1
-- [1,2,3,4]
-- >>> L.sort $ edges g1
-- [(1,2),(1,3),(2,4),(3,4)]
gridG :: Int -> Int -> Graph
gridG n m = fromEdges (half1 ++ half2)
  where half1 = [(x,x+1) | x <- [1..row*col], x `mod` col /= 0]
        half2 = [(x,x+col) | x <- [1..row*col], x+col <= row*col]
        row = n + 1
        col = m + 1

tarjan :: Graph
tarjan = buildG (1, 16) [(1,2),(1,12),(2,3),(2,16),
                         (3,16),(3,4),(4,5),(4,7),
                         (5,6),(5,15),(6,7),(6,8),
                         (7,8),(8,9),(9,10),(9,15),
                         (10,11),(10,16),(11,12),(11,14),
                         (12,13),(13,14),(13,15),(14,15)]

-- |Build a grid with its faces
--
-- >>> L.sort $ snd $ gridGWithFaces 2 2
-- [[(1,2),(1,4),(2,5),(4,5)],[(2,3),(2,5),(3,6),(5,6)],[(4,5),(4,7),(5,8),(7,8)],[(5,6),(5,8),(6,9),(8,9)]]
-- >>> L.sort $ snd $ gridGWithFaces 1 1
-- [[(1,2),(1,3),(2,4),(3,4)]]
gridGWithFaces :: Int -> Int -> (Graph, Faces)
gridGWithFaces n m = (graph, faces)
    where graph = gridG n m
          row = n+1
          col = m+1
          faces = do
            (p1,p2,p3,p4) <- [(x,x+1,x+col,x+col+1) | x <- [1..row*col]]
            guard (p1 `mod` col /= 0)
            guard (p3 < row*col)
            return [(p1,p2), (p1,p3), (p2,p4), (p3,p4)]

-- |Returns the canonical labeling of a graph.
canonize :: Graph -> Graph
canonize g = Graph { inner=g' } 
  where g' = canonicGraph part (inner g)
        part = unitPartition (lohi g)

automorphisms :: Graph -> [Permutation]
automorphisms g = autGenerators part (inner g)
    where part = unitPartition (lohi g)

edges :: Graph -> [Edge]
edges (Graph g) = [(u,v) | (u,v) <- G.edges g, u < v]

-- |Same as `edges`, except it doesn't filter any edge
edges2 :: Graph -> [Edge]
edges2 (Graph g) = G.edges g 

edgesOf :: Vertex -> Graph -> [Edge]
edgesOf v (Graph g) = L.sortBy (compare `on` snd) [(x,y) | (x,y) <- G.edges g, x == v]  

lohi :: Graph -> Bounds
lohi g = (L.minimum vs, L.maximum vs)
  where vs = vertices g

vertices :: Graph -> [Vertex]
vertices = G.vertices.inner

fromEdges :: [Edge] -> Graph
fromEdges es = buildG (1, max) es
  where max = maximum $ L.concat [[a,b] | (a,b) <- es]

-- |Build a graph from undirected edges.
buildG :: Bounds -> [Edge] -> Graph
buildG bnd es = Graph { inner=G.buildG bnd (es ++ es') }
  where es' = map swap es

-- |Returns vertex's neighbors.
neighbors :: Graph -> Vertex -> [Vertex]
neighbors = (A.!).inner

-- |Returns graph's faces
-- Preconditions : 
--      - Graph must be planar
--      - Graph must be in a planar embedding
faces :: Graph -> Faces
faces g = []    -- TODO

-- |Removes an undirected edge.
--
-- >>> let g1 = gridG 1 1
-- >>> L.sort $ edges $ rmEdge (1,2) g1
-- [(1,3),(2,4),(3,4)]
-- >>> edges $ foldr rmEdge g1 $ edges g1
-- []
rmEdge :: Edge -> Graph -> Graph
rmEdge (u,v) g = Graph{ inner=g' }
  where g' = (inner g) A.// [(u,nbu), (v, nbv)] -- Incrementally update Graph
        nbu = L.delete v $ neighbors g u  -- Remove v from u's neighbors
        nbv = L.delete u $ neighbors g v  -- Remoev u from v's neighbors
