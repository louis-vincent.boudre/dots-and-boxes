{-# LANGUAGE TemplateHaskell #-}

module Planar
    (
    ) where


import Graph
import Data.Array
import Data.Function (on)
import Data.List (sortBy, reverse)
import Data.Sequence (Seq, (|>), empty)
import Control.Lens.TH
import Control.Lens.Operators ((%~), (^.), (.~), (&))
import Data.Foldable (toList)

type LOWPT = (Int, Int) -- (LOWPT1, LOWPT2)
data EdgeType = Arc | Frond deriving (Show, Eq)
type NumberMap = Array Vertex Int 
type LowptsMap = Array Vertex LOWPT 
type MarkedEdge = (Vertex, Vertex, EdgeType)

type PalmTree = Array Vertex [(Vertex, EdgeType)]
type Path = Seq Edge

data DfsState = DfsState {
    _n :: Int,
    _numberMap :: Array Vertex Int,
    _lowptsMap :: Array Vertex LOWPT,
    _palmTree :: PalmTree, 
    _graph :: Graph
} 

data PathFState = PState {
    _palmTree2 :: PalmTree,
    _paths :: Seq Path,
    _path :: Path
} deriving (Show)

initDFS :: Graph -> DfsState
initDFS graph = DfsState { 
        _n=0, 
        _numberMap = array (lo,hi) [(i, 0)  | i <- [1..hi]],
        _lowptsMap = array (lo,hi) [(i, (0,0))  | i <- [1..hi]],
        _palmTree  = array (lo,hi) [(i, []) | i <- [1..hi]], 
        _graph = graph
    }
    where (lo,hi) = lohi graph

pathSFromPTree :: PalmTree -> PathFState
pathSFromPTree pt = PState {_palmTree2=pt, _paths=empty, _path=empty} 

makeLenses ''DfsState
makeLenses ''PathFState

updateMaps :: Vertex -> DfsState -> DfsState
updateMaps v state = state
    & n .~ n'
    & numberMap %~ (\old -> old // [(v, n')])
    & lowptsMap %~ (\old -> old // [(v, (n', n') )]) 
    where n' = (state ^. n) + 1

number :: DfsState -> Vertex -> Int
number state v = (state ^. numberMap) ! v 

lowpt1 :: DfsState -> Vertex -> Int
lowpt1 state v = fst $ (state ^. lowptsMap) ! v

lowpt2 :: DfsState -> Vertex -> Int
lowpt2 state v = snd $ (state ^. lowptsMap) ! v

updateDispatcher :: Vertex -> Edge -> DfsState -> DfsState 
updateDispatcher father (v,w) state = fn (v,w) state 
    where nop _ x = x
          number' = number state
          fn = if (number' w) == 0
                then markArc
                else if (number' w) < (number' v) && w /= father
                    then markFrond 
                    else nop 

mark :: EdgeType -> Edge -> DfsState -> DfsState
mark edgeType (v,w) state = state
    & palmTree .~ (state ^. palmTree) // [( v, (w, edgeType):adjacencyList )]
    where adjacencyList = (state ^. palmTree) ! v

markArc :: Edge -> DfsState -> DfsState
markArc (v,w) state = state' 
    & lowptsMap .~ (state' ^. lowptsMap) // [(v, (pt1, pt2))]   
    where state' = dfs' (w,v) $ mark Arc (v,w) state
          lowpt1' = lowpt1 state'
          lowpt2' = lowpt2 state'
          (pt1,pt2) = if (lowpt1' w) < (lowpt1' v)
                        then (lowpt1' w, min (lowpt1' v) (lowpt2' w))
                        else if (lowpt1' w) == (lowpt1' v)
                            then (lowpt1' v, min (lowpt2' v) (lowpt2' w))
                            else (lowpt1' v, min (lowpt2' v) (lowpt1' w))

markFrond :: Edge -> DfsState -> DfsState
markFrond (v,w) state = mark Frond (v,w) $ state
    & lowptsMap .~ (state ^. lowptsMap) // [(v, (pt1, pt2))]
    where lowpt1' = lowpt1 state
          lowpt2' = lowpt2 state
          number' = number state
          (pt1, pt2) = if (number' w) < (lowpt1' v) 
                        then (number' w, lowpt1' v)
                        else if (number' w) > (lowpt1' v)                                
                            then (lowpt1' v, min (lowpt2' v) (number' w))
                            else (lowpt1' v, lowpt2' v)

sortAdjacencyList :: DfsState -> PalmTree
sortAdjacencyList state = pt // (map (sortByPhi lmap) $ assocs pt)
    where lmap = state ^. lowptsMap
          pt = state ^. palmTree
          
          
sortByPhi :: LowptsMap -> (Vertex, [(Vertex, EdgeType)]) -> (Vertex, [(Vertex, EdgeType)])
sortByPhi lmap (v, adj) = (v, adj')
    where adj' = sortBy (compare `on` (phi lmap v)) adj

-- |Adds a score to an edge
-- This function is used for sorting 
phi :: LowptsMap -> Vertex -> (Vertex, EdgeType) -> Int
phi _ v (w, Frond)  = 2 * w                             
phi lmap v (w, Arc)
    | pt2 >= v = 2 * pt1
    | otherwise = 2 * pt1 + 1
        where (pt1, pt2) = lmap ! w

-- This function is used for sorting 
phi2 :: LowptsMap -> (Vertex, Vertex, EdgeType) -> Int
phi2 _ (v, w, Frond)  = 2 * w                
phi2 lmap (v, w, Arc)
    | pt2 >= v = 2 * pt1
    | otherwise = 2 * pt1 + 1
        where (pt1, pt2) = lmap ! w

sortByPhi2 :: DfsState -> PalmTree
sortByPhi2 state = fmap reverse pt' 
    where pt = state ^. palmTree
          (_, hi) = lohi $ state ^. graph
          lmap = state ^. lowptsMap
          buckets = accumArray (|>) empty (1, 2*hi + 1) [(phi2 lmap e, e) | e <- edges]
          edges = concat [[(v,w,et) | (w,et) <- adj] | (v, adj) <- assocs pt]
          pt' = accumArray (flip (:)) [] (1,hi) $ concat [[(v,(w,et)) | (v,w,et) <- bucket] | i <- [1..2*hi+1], let bucket = toList $ buckets ! i] 

dfs' :: Edge -> DfsState -> DfsState
dfs' (v, u) state = foldr (updateDispatcher u) state' $ edgesOf v g
    where state' = updateMaps v state
          g = state ^. graph

-- | map each vertex `v` to  `NUMBER(v)`
mapNumberPTree :: DfsState -> PalmTree
mapNumberPTree state = array bnds [(nmap ! i, map numberNeighbor' e) | (i,e) <- assocs pt]
    where numberNeighbor' (e, etype) = (nmap ! e, etype)
          bnds = bounds pt
          nmap = state ^. numberMap
          pt = state ^. palmTree

mapNumberLMap :: DfsState -> LowptsMap
mapNumberLMap state = array bnds lmap'
    where bnds = bounds lmap
          lmap'= [(nmap ! i, (nmap ! x, nmap ! y)) | (i, (x,y)) <- assocs lmap]
          lmap = state ^. lowptsMap
          nmap = state ^. numberMap

reverseNumberMap :: NumberMap -> NumberMap
reverseNumberMap nmap = array (bounds nmap) $ [(y,x) | (x,y) <- assocs nmap]

-- |Changes every vertex `v` to `NUMBER(v)`
mapNumber :: DfsState -> DfsState
mapNumber state = state
    & palmTree  .~ mapNumberPTree state
    & lowptsMap .~ mapNumberLMap state
    & numberMap %~ reverseNumberMap

-- |Build a palm tree from a graph
--
-- >>> assocs $ dfs $ gridG 1 1
-- [(1,[(2,Arc)]),(2,[(3,Arc)]),(3,[(4,Arc)]),(4,[(1,Frond)])] 
-- >>> assocs $ dfs tarjan
-- []
dfs :: Graph -> PalmTree
dfs g = sortAdjacencyList $ mapNumber state
    where state = dfs' (1,0) $ initDFS g

pathfinder' :: (Vertex, Vertex, EdgeType) -> PathFState -> PathFState
pathfinder' (v,w,Arc) state = pathfinder w $ state 
    & path %~ (flip (|>)) (v,w)
pathfinder' (v,w,Frond) state = state
    & paths %~ (flip (|>)) path'
    & path .~ empty
    where path' = (state ^. path) |> (v,w)

-- |Find all paths in a Palm Tree
--
-- >>> let ps = pathSFromPTree (dfs tarjan)
-- >>> cleanPath $ pathfinder 1 ps
-- []
pathfinder :: Vertex -> PathFState -> PathFState
pathfinder v state = foldr pathfinder' state  $ map (\tpl -> (v, fst tpl, snd tpl)) adj 
    where adj = (state ^. palmTree2) ! v

cleanPath :: PathFState -> [[Vertex]]
cleanPath state = map f $ map toList $ toList (state ^. paths)
    where f [(x,y)] = [x,y]
          f ((x,y):(z,w):es)
            | y == z = x : f ((z,w):es)
            | otherwise = x : y : f ((z,w):es)
