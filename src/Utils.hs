module Utils
    (
        trd,
        if',
        pickAny,
        pickAndUpdate
    ) where

import Data.Foldable (find)
import Data.Maybe (fromJust)
import Data.HashSet
import Data.Hashable
import Data.Array

trd :: (a, b, c) -> c
trd (_,_,x) = x

if' :: Bool -> a -> a -> a
if' True x _ = x
if' False _ y = y

-- |Pick first element from a foldable structure.
-- panics if the structure is empty
pickAny :: (Foldable f) => f a -> a
pickAny = fromJust . find (\_ -> True)


pickAndUpdate :: (Hashable a, Eq a) => HashSet a -> (a, HashSet a)
pickAndUpdate set = let x = pickAny set in (x, delete x set)

