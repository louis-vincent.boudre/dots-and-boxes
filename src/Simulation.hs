module Simulation
    (
        Simulation(..),
        exec,
        isFinished,
        step,
        rndStrategy,
        buildRandomSim
    ) where

import Game
import Graph
import Box
import Utils
import System.Random

-- |Simulate one turn.
step :: Simulation -> Simulation
step sim@(Sim game f g) =
    if player game == X
        then f sim
        else g sim

data Simulation = Sim {
    game :: Game,
    xstrat :: (Simulation -> Simulation),
    ystrat :: (Simulation -> Simulation)
}

buildRandomSim :: (Int, Int) -> Game -> Simulation
buildRandomSim (seed1, seed2) game = (Sim game f g)
    where gen1 = mkStdGen(seed1) 
          gen2 = mkStdGen(seed2)    
          f = rndStrategy gen1
          g = rndStrategy gen2

isFinished :: Simulation -> Bool
isFinished (Sim game _ _) = ((==) 0).length $ moves game

-- |Simulation a game and returns the win margin
exec :: Simulation -> Int
exec sim@(Sim game _ _)
    | isFinished sim = winMargin game
    | otherwise = exec (step sim)


randomMove :: RandomGen g => [Edge] -> g -> (Edge, g)
randomMove es gen = (head $ drop n es, gen')
    where (lo,hi) = (0, (length es) - 1)
          (n, gen') = randomR (lo,hi) gen 

-- | Random strategy
rndStrategy :: RandomGen g => g -> Simulation -> Simulation
rndStrategy gen sim@(Sim game f g) = (Sim game' f' g')
    where (edge, gen') = randomMove (moves game) gen
          h = rndStrategy gen'
          f' = if' ((player game) == X) h f
          g' = if' ((player game) == Y) h g
          game' = updateGame edge game

