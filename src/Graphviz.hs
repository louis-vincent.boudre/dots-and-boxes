module Graphviz
    (
        Graphviz(..)
    ) where

import Data.List



class Graphviz g where
    
    headerStr :: g -> String
    headerStr _ = "graph {\n\tmargin=0;\n\tnode [fixedsize=true, width=0.3, height=0.3, style=filled];"
    
    footerStr :: g -> String
    footerStr _ = "}"
    
    nodesStr :: g -> String

    edgesStr :: g -> String

    showGraphviz :: g -> String
    showGraphviz g = intercalate "\n" [headerStr g, nodesStr g, edgesStr g, footerStr g]
    
