
- Si l'adversaire est susceptible de créer une victoire (une chaîne ou une 
boîte gagnante), le seul coup optimal est d'empêcher cette victoire. 
- double-cross strategy : supposons que nous avons une chaîne à moitié ouverte
et une chaîne complètement ouverte donnant plus de points que la première,
le joueur A doit sacrifier la première chaîne pour le forcer à ouvrir la meilleure.

```
+-+ + +
| | | |
+ +-+ +
| |   |
+ + +-+
| |
+ +-+-+

+-+-+ +
|A|A| |
+-+-+ +
| |   |
+ + +-+
| |
+-+-+-+   <-- A donne deux boîtes == hard hearted box

+-+-+ +
|A|A| |
+-+-+ +
|B| | |   <-- B est forcé de démarrer une nouvelle chaîne
+-+ +-+
|B|
+-+-+-+

+-+-+-+
|A|A|A|
+-+-+-+
|B|A|A|   
+-+-+-+
|B|A|A|
+-+-+-+
```

Le joueur qui ouvre la première chaîne ou boucle va perdre.

# Doublecross

Lorsque l'on donne à l'opposant deux boîtes pouvant être fermé à l'aide d'une
arrête.

# Doubletrap

Donner à l'adversaire une 2-chain ce qui lui offre la possibilité de l'accepté
ou d'effectuer un doublecross.
Stratégie risquer : peu intéressante pour une partie optimale, par contre permet
de changer la parité dans la partie si le joueur effectue un doublecross.

# Chain

Une chaîne est une suite de 3 boîtes ou plus.
Une chaîne est souvent appelé "Long chain" dans les articles scientifiques.

Une chaîne de 2 boîtes ou moins est appelé "Short Chain", sauf certains articles
ne les considèrent pas comme des chaînes, dans le sens où elle n'affecte pas
la parité du nombre de chaîne.

# Cycle

Un cycle est une boucle fermé avec 4 boîtes ou plus.

# Chain rule

Si le nombre de sommets est impair le premier joueur devrait essayer de faire un nombre impair de
chaîne et le deuxième un nombre pair.
L'inverse si le nombre de sommets est pair. P1 = pair, P2 = impair.

# Chain fight

Dans une partie, chaque joueur doit se battre pour garder le nombre de chaîne consistente.

